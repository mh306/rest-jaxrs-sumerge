package org;


import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class PageNotFoundExceptionWrapper implements ExceptionMapper<Throwable> {

    @Override
    public Response toResponse(Throwable throwable) {
        System.out.println("Page not found");
        return Response.status(Response.Status.NOT_FOUND).entity("PAGE NOT FOUND !! 404").build();
    }
}
