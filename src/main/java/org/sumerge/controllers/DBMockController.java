package org.sumerge.controllers;

import org.sumerge.models.ClassModel;
import org.sumerge.models.StudentModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DBMockController {

    private static Map<Long, StudentModel> StudentList = new HashMap<Long, StudentModel>();
    private static Map<Long, ClassModel> ClassList = new HashMap<Long, ClassModel>();

    public static Map<Long, StudentModel> GetDBStudents (){
        return StudentList;
    }

    public static Map<Long, ClassModel> GetDBClasses()
    {
        return ClassList;
    }

}
