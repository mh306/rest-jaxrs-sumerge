package org.sumerge.services;

import org.sumerge.controllers.DBMockController;

import org.sumerge.models.StudentModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class StudentService {

    private Map<Long, StudentModel> StudentList = DBMockController.GetDBStudents();

    public StudentService(){
        StudentList.put((long) (StudentList.size()+1),new StudentModel(1152056,"Haitham",2015,"CCE"));
        StudentList.put((long) (StudentList.size()+1),new StudentModel(1355,"Mohamed",2020,"MDE"));
        StudentList.put((long) (StudentList.size()+1),new StudentModel(1234,"Hany",2000,"EEE"));

    }
    public StudentModel GetStudentByID(Long ID){
        return StudentList.get(ID);

    }

    public List<StudentModel> GetAllStudents(){
        return new ArrayList<StudentModel>(StudentList.values());
    }

    public StudentModel AddStudent(StudentModel NewStudent){
        NewStudent.setID(StudentList.size()+1);
        StudentList.put(NewStudent.getID(),NewStudent);
        return NewStudent;
    }

    public List<StudentModel> GetAllStudentsByYear(int year){
        List<StudentModel> YearStudents = new ArrayList<StudentModel>();
        for(StudentModel Student : StudentList.values()){
            if(Student.getYear() == year)
                YearStudents.add(Student);
        }
        return YearStudents;
    }
    public StudentModel UpdateStudent(StudentModel NewStudent){
        if( NewStudent.getID()<=0)
                return null;
        else
        {
            StudentList.put(NewStudent.getID(),NewStudent);
            return NewStudent;
        }
    }

    public StudentModel DeleteStudent(Long ID){
        return StudentList.remove(ID);
    }



}
