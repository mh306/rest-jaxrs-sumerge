package org.sumerge.services;

import org.sumerge.controllers.DBMockController;
import org.sumerge.models.ClassModel;
import org.sumerge.models.StudentModel;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ClassService {

    private Map<Long, ClassModel> ClassList = DBMockController.GetDBClasses();

    public ClassService(){

        List<StudentModel> ClassStudent = new ArrayList<StudentModel>();

        ClassStudent.add(new StudentModel(1152056,"Haitham",2015,"CCE"));
        ClassStudent.add(new StudentModel(1152056,"Mohamed",2015,"CCE"));
        ClassStudent.add(new StudentModel(1152056,"Hany",2015,"CCE"));

        ClassList.put((long) (ClassList.size()+1), new ClassModel(1,"Economics",ClassStudent));
    }
    public ClassModel GetClassByID(Long ID) {
        return ClassList.get(ID);

    }

    public List<ClassModel> GetAllClasses() {
        return new ArrayList<ClassModel>(ClassList.values());
    }

    public List<ClassModel> GetAllClassesPaginated(int start,int size){
        ArrayList<ClassModel> List = new ArrayList<ClassModel>(ClassList.values());
        if(start+size >List.size())
            return new ArrayList<ClassModel>();
        else
            return List.subList(start,start+size);

    }

    public ClassModel AddClass(ClassModel NewClass) {
        NewClass.setID(ClassList.size() + 1);
        ClassList.put(NewClass.getID(), NewClass);
        return NewClass;
    }

    public ClassModel UpdateClass(ClassModel NewClass) {
        if (NewClass.getID() <= 0)
            return null;
        else {
            ClassList.put(NewClass.getID(), NewClass);
            return NewClass;
        }
    }

    public ClassModel DeleteClass(Long ID) {
        return ClassList.remove(ID);
    }
}
