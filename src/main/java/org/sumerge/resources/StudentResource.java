package org.sumerge.resources;

import org.sumerge.models.ClassModel;
import org.sumerge.models.StudentModel;
import org.sumerge.services.ClassService;
import org.sumerge.services.StudentService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/student")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class StudentResource implements  CrudStudent {

    StudentService studentService = new StudentService();


    public StudentModel GetResourceByID(Long ID) {
        StudentModel Student = studentService.GetStudentByID(ID);
        if (Student == null){
            throw new NotFoundException("Student Not Found !!");
        }else

            return Student ;

    }

    public List<StudentModel> GetAllResources(int year) {

        if (year >0){
            return studentService.GetAllStudentsByYear(year);
        }
        return studentService.GetAllStudents();
    }


    public StudentModel AddResource(StudentModel NewStudent) {
        return studentService.AddStudent(NewStudent);
    }


    public StudentModel UpdateResource( Long ID, StudentModel NewStudent) {
        NewStudent.setID(ID);
        return studentService.UpdateStudent(NewStudent);
    }


    public StudentModel DeleteResource(Long ID) {
        return studentService.DeleteStudent(ID);
    }
}
