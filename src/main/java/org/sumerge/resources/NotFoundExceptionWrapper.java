package org.sumerge.resources;

import org.sumerge.resources.NotFoundException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class NotFoundExceptionWrapper implements ExceptionMapper<NotFoundException> {

    @Override
    public Response toResponse(NotFoundException e) {
        System.out.println("Res not found ");
        return Response.status(Response.Status.NOT_FOUND).entity(e.getMessage()).build();
    }
}
