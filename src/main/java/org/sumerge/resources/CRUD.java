package org.sumerge.resources;

import org.sumerge.models.ClassModel;

import javax.ws.rs.*;
import java.util.List;

public interface CRUD {

    @GET
    @Path("/{ID}")
    public ClassModel GetResourceByID(@PathParam("ID") Long ID);

    @GET
    public List<ClassModel> GetAllResources(@QueryParam("start")int start, @QueryParam("size") int size) ;

    @POST
    public ClassModel AddResource(ClassModel NewClass);

    @PUT()
    @Path("/{ID}")
    public ClassModel UpdateResource(@PathParam("ID") Long ID, ClassModel NewClass);

    @DELETE
    @Path("/{ID}")
    public ClassModel DeleteResource(@PathParam("ID") Long ID);
}
