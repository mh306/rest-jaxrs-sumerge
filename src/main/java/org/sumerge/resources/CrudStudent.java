package org.sumerge.resources;

import org.sumerge.models.ClassModel;
import org.sumerge.models.StudentModel;

import javax.ws.rs.*;
import java.util.List;

public interface CrudStudent {

    @GET
    @Path("/{ID}")
    public StudentModel GetResourceByID(@PathParam("ID") Long ID);

    @GET
    public List<StudentModel> GetAllResources(@QueryParam("year")int year) ;

    @POST
    public StudentModel AddResource(StudentModel NewClass);

    @PUT()
    @Path("/{ID}")
    public StudentModel UpdateResource(@PathParam("ID") Long ID, StudentModel NewStudent);

    @DELETE
    @Path("/{ID}")
    public StudentModel DeleteResource(@PathParam("ID") Long ID);

}
