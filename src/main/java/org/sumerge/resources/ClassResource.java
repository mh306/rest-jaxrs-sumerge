package org.sumerge.resources;

import org.sumerge.models.ClassModel;
import org.sumerge.services.ClassService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@Path("/class")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ClassResource implements  CRUD {

    ClassService classService = new ClassService();


    public ClassModel GetResourceByID( Long ID) {
        ClassModel Class = classService.GetClassByID(ID);
        if (Class == null){
            throw new NotFoundException("Class Not Found !!");
        }else

        return Class ;

    }

    public List<ClassModel> GetAllResources(@QueryParam("start")int start, @QueryParam("size") int size) {

        if (start >0  && size > 0){
            return classService.GetAllClassesPaginated(start,size);
        }
        return classService.GetAllClasses();
    }


    public ClassModel AddResource(ClassModel NewClass) {
        return classService.AddClass(NewClass);
    }


    public ClassModel UpdateResource( Long ID, ClassModel NewClass) {
        NewClass.setID(ID);
        return classService.UpdateClass(NewClass);
    }


    public ClassModel DeleteResource(Long ID) {
        return classService.DeleteClass(ID);
    }
}
