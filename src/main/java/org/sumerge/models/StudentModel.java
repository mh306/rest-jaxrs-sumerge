package org.sumerge.models;

public class StudentModel {

    private long ID;
    private String Name;
    private int Year;
    private String Major;

    public StudentModel(long ID, String name, int year, String major) {
        this.ID = ID;
        Name = name;
        Year = year;
        Major = major;
    }


    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getYear() {
        return Year;
    }

    public void setYear(int year) {
        Year = year;
    }

    public String getMajor() {
        return Major;
    }

    public void setMajor(String major) {
        Major = major;
    }
}

