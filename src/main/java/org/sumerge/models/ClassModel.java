
package org.sumerge.models;

        import java.util.List;

public class ClassModel {


    public ClassModel(long ID, String name, List<StudentModel> studentList) {
        this.ID = ID;
        this.Name = name;
        this.studentList = studentList;
    }

    private  long ID;
    private String Name;
    private List<StudentModel> studentList;

    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public List<StudentModel> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<StudentModel> studentList) {
        this.studentList = studentList;
    }
}
