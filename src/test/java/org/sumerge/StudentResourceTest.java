package org.sumerge;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.internal.matchers.Not;
import org.sumerge.models.StudentModel;
import org.sumerge.resources.NotFoundException;
import org.sumerge.resources.StudentResource;
import org.sumerge.services.StudentService;

import java.util.List;

import static groovy.xml.Entity.times;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class StudentResourceTest {


    StudentResource stdres;

    //init for common objects

    @BeforeEach
    public void setup(){
        stdres = Mockito.spy(StudentResource.class);

    }
    @Test
    @DisplayName("Get a student that doesn't exist by id ")
    public void GetStudentByIDFail(){
        assertThrows(NotFoundException.class, ()->{stdres.GetResourceByID((long) 88888);},"Student ID doesnt exist");
    }

    @Test
    @DisplayName("Dummy to try verify and when return syntax")
    public void dummy(){
        StudentModel expected = new StudentModel(1,"Haitham",2015,"CCE");
        StudentResource stdresmock = Mockito.mock(StudentResource.class);

        when(stdresmock.GetResourceByID((long) 1)).thenReturn(expected);

        StudentModel actual = stdresmock.GetResourceByID((long) 1);
        StudentModel actual2 = stdresmock.GetResourceByID((long) 1);

        verify(stdresmock,times(2)).GetResourceByID((long)1);


    }
    @Test
    @DisplayName("Get a student who exists by id ")
    public void GetStudentByID(){
        StudentModel expected = new StudentModel(1,"Haitham",2015,"CCE");
        StudentModel actual = stdres.GetResourceByID((long) 1);
        assertEquals(expected.getYear(),actual.getYear());
        assertEquals(expected.getMajor(),actual.getMajor());
        assertEquals(expected.getName(),actual.getName());
        assertEquals(expected.getID(),actual.getID());


    }

    @Test
    @DisplayName("Get students by year ")
    public void GetStudentsBYYear(){
        List<StudentModel> list = stdres.GetAllResources(2015);
        list.forEach(
                std->{
                    assertEquals(2015,std.getYear());
        });
    }
    @Test
    @DisplayName("adding a student test")
    public void AddStudent(){
        StudentModel tobeadded = new StudentModel((long)0,"Haitham",2015,"CCE");
        StudentModel returned = stdres.AddResource(tobeadded);

        assertEquals(tobeadded.getYear(),returned.getYear());
        assertEquals(tobeadded.getMajor(),returned.getMajor());
        assertEquals(tobeadded.getName(),returned.getName());


    }

    @Test
    @DisplayName("Update Student who exists")
    public void UpdateStudent(){
        //student exists in dummy service at launch for testing
        StudentModel tobeadded = new StudentModel(1,"Haitham",2015,"CCE");
        StudentModel returned = stdres.UpdateResource(tobeadded.getID(),tobeadded);

        assertEquals(tobeadded.getYear(),returned.getYear());
        assertEquals(tobeadded.getMajor(),returned.getMajor());
        assertEquals(tobeadded.getName(),returned.getName());

    }


    @Test
    @DisplayName("Update Student  who doesn't exist")
    public void UpdateStudentFail(){
        //student does not exist
        StudentModel tobeadded = new StudentModel(5648,"Haitham",2015,"CCE");
        assertThrows(NotFoundException.class,()->{stdres.UpdateResource(tobeadded.getID(),tobeadded);},"Student does not exist ");

    }


}
